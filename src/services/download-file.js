const util = require('util');
const stream = require('stream');
const axios = require('axios')
const fs = require('file-system')
const pipeline = util.promisify(stream.pipeline);

const downloadFile = async (url) => {
  try {
    const request = await axios.get('url', {
      responseType: 'stream',
    });
    await pipeline(request.data, fs.createWriteStream('/temp/my.pdf'));
    console.log('download pdf pipeline successful');   
  } catch (error) {
    console.error('download pdf pipeline failed', error);
  }
}

exports.downloadFile = downloadFile