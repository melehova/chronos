import axios from 'axios';
import authHeader from './auth-header';
import authService from './auth.service';
import qs from 'qs';

const API_URL = 'http://localhost:4000/api/';

class UserService {
  likePost(post_id, type) {
    let user = authService.getCurrentUser()
    const data = {
      author: user._id,
      publishDate: new Date(),
      relatedToIdModel: 'Post',
      type: type
    }
    return axios({
      method: "post",
      url: API_URL + 'posts/' + post_id + '/like',
      data: qs.stringify(data),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': authHeader()['x-access-token']
      },
    })
  }
  getAllUsers() {
    // return axios.get(API_URL + 'users/', { headers: authHeader() });
    return axios.get(API_URL + 'users/profile');
  }

  async getPostComments(post_id) {
    return await axios.get(API_URL + 'posts/' + post_id + '/comments', { headers: authHeader() })
  }

  async getPostLikes(post_id) {
    return await axios.get(API_URL + 'posts/' + post_id + '/like', { headers: authHeader() })
  }

  getAllPosts() {
    return axios.get(API_URL + 'posts/', { headers: authHeader() });
  }

  getAllCategories() {
    return axios.get(API_URL + 'categories/', { headers: authHeader() })
  }

  async getUser(user_id) {
    return await axios.get(API_URL + 'users/' + user_id, { headers: authHeader() })
  }

  postNewPost(title, content, categories) {
    let user = authService.getCurrentUser()
    const data = {
      title,
      content,
      categories: categories,
      author: user._id,
      publishDate: new Date(),
      status: 'active'
    }
    return axios({
      method: "post",
      url: API_URL + 'posts/',
      data: qs.stringify(data, { encode: false, indices: false, arrayFormat: 'repeat' }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': authHeader()['x-access-token']
      },
    })
  }

  postNewComment(post_id, content) {
    let user = authService.getCurrentUser()
    const data = {
      content,
      author: user._id,
      publishDate: new Date(),
      relatedToIdModel: "Post"
    }
    return axios({
      method: "post",
      url: API_URL + 'posts/' + post_id + '/comments',
      data: qs.stringify(data, { encode: false, indices: false, arrayFormat: 'repeat' }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': authHeader()['x-access-token']
      },
    })
  }

  postNewCategory(title, description) {
    const data = {
      description,
      title
    }
    return axios({
      method: "post",
      url: API_URL + 'categories/',
      data: qs.stringify(data, { encode: false, indices: false, arrayFormat: 'repeat' }),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        'x-access-token': authHeader()['x-access-token']
      },
    })
  }

  deletePost(post_id) {
    return axios.delete(API_URL + 'posts/' + post_id, { headers: authHeader() })
  }

  getPost(post_id) {
    return axios.get(API_URL + 'posts/' + post_id, { headers: authHeader() })
  }

  // getUserBoard() {
  //   return axios.get(API_URL + 'user', { headers: authHeader() });
  // }

  // getModeratorBoard() {
  //   return axios.get(API_URL + 'mod', { headers: authHeader() });
  // }

  // getAdminBoard() {
  //   return axios.get(API_URL + 'admin', { headers: authHeader() });
  // }
}

export default new UserService();
