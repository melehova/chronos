import axios from "axios";
import iso_to_gcal from '../utils/iso_to_gcal'
axios.defaults.withCredentials = false;

const API_LINK = 'http://localhost:4000/api/calendars'
const GOOGLE_BASE_CALENDAR_URL = "https://www.googleapis.com/calendar/v3/calendars";

class CalendarService {
    async getCalendarRegion() {
        return fetch("https://ipapi.co/json/", {
            credentials: 'omit'
        })
            .then(resp => resp.json())
            .then(data => {
                localStorage.setItem("iso", data.country_code.toLowerCase());
                return {
                    region: data.country_code.toLowerCase(),
                    language: data.languages.split(',')[0]
                }
            })
            .catch(err => {
                console.error(err)
                return err
            })
    }

    async getNationalHolidays(iso = localStorage.getItem('iso') || 'ua', language = 'en') {
        console.log(iso)
        const region = iso_to_gcal[iso]
        const BASE_CALENDAR_ID_FOR_PUBLIC_HOLIDAY =
            "holiday@group.v.calendar.google.com"; // Calendar Id. This is public but apparently not documented anywhere officialy.
        const CALENDAR_REGION = [language, region].join('.');
        const url = `${GOOGLE_BASE_CALENDAR_URL}/${CALENDAR_REGION}%23${BASE_CALENDAR_ID_FOR_PUBLIC_HOLIDAY}/events?key=${process.env.REACT_APP_GOOGLE_API_KEY}`
        return fetch(url, {
            credentials: 'omit'
        })
            .then(resp => resp.json())
            .then(data => {
                const user = JSON.parse(localStorage.getItem('user'))
                user.calendars.push({ calendar_id: `${CALENDAR_REGION}#${BASE_CALENDAR_ID_FOR_PUBLIC_HOLIDAY}`, role: 'GUEST', title: data.items[0].creator.displayName })
                localStorage.setItem('user', JSON.stringify(user))
                return data.items
            })
    }
    async getMyCalendars() {
        // return 'TODO getMyCalendars' 
        return axios.get(API_LINK)
            .then(res => {
                return res
            })
    }

    async getEvent(calendar_id, event_id, calendar_type) {
        switch (calendar_type) {
            case 'national':
                // without replacing it requires authentification
                const url = `${GOOGLE_BASE_CALENDAR_URL}/${calendar_id.replace('#', '%23')}/events/${event_id}?key=${process.env.REACT_APP_GOOGLE_API_KEY}`
                return fetch(url, {
                    credentials: 'omit'
                })
                    .then(resp => resp.json())
                    .then(ev => {
                        return {
                            id: ev.id,
                            start: new Date((new Date(ev.start.date)).toISOString().slice(0, -1)),
                            end: new Date((new Date(ev.end.date)).toISOString().slice(0, -1)),
                            color: '#F66082',
                            allDay: true,
                            title: ev.summary,
                            description: ev.description.split('\n')[0],
                            visibility: ev.visibility,
                            calendar: ev.organizer.displayName,
                            editable: false,
                            calendar_id: ev.creator.email,
                            calendar_type: 'national'
                        }
                    })
            case 'u':
                break
            default:
                break
        }
        return axios.get(API_LINK)
            .then(res => {
                console.log(res)
                //// TODO
            })
    }
}

export default new CalendarService()