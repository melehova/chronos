import axios from "axios";
axios.defaults.withCredentials = true;

const API_URL = "http://localhost:4000/api/auth/";

class AuthService {
  async login(login, password) {
    const response = await axios
      .post(API_URL + "login", {
        login,
        password
      });
    // console.log(response.status === 200)
    if (response.status === 200) {
      localStorage.setItem("user", JSON.stringify(response.data.user));
    }
    // return response.data;
  }

  logout() {
    localStorage.removeItem("user");
  }

  register(login, email, password, fullname, confirmation_password) {
    return axios.post(API_URL + "register", {
      login,
      email,
      password,
      fullname,
      confirmation_password
    });
  }


  async refreshToken() {
    axios.post(API_URL + "refresh", { withCredentials: true })
      .then(response => {
        console.log(response.headers['set-cookie'])
        if (response.data.token) {
          const user = localStorage.getItem('user')
          user.token = response.data.token
          localStorage.setItem("user", JSON.stringify(user));
        }
      },
        error => {
          console.error(error)
        })

  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));;
  }
}

export default new AuthService();
