// import PropTypes from 'prop-types'
import { Icon } from '@iconify/react';
import { Fragment, useMemo, useState } from 'react';
import './SelectedEvent.css'
import moment from 'moment'
import { momentLocalizer } from 'react-big-calendar'
import { Link } from "react-router-dom";
import { Telegram } from 'react-social-sharing'
import DropdownComp from './dropdown.component';

const localizer = momentLocalizer(moment)

const SeBlock = ({ title, icon, text, color }) => {
    return (
        <div className='se-block'>
            <div className='se-marker'>
                {color &&
                    <div className='se-color-marker' style={{ borderColor: color }}></div>
                }
                {icon &&
                    <Icon icon={icon} className='se-icon' />
                }
            </div>
            <div className='se-block-content'>
                {title &&
                    <div className='se-title'>{title}</div>
                }
                <div className='se-text-list'>
                    {text && text.map((item, idx) => (
                        <div className='se-text' key={idx}>{item}</div>
                    ))}
                </div>
            </div>
        </div>
    )
}

const SelectedEvent = ({ event, onClose, alert }) => {
    const { id, color, title, start, end, allDay, repeat, reminders, calendar_id, calendar_type = 'u', calendar = 'Personal calendar', editable = true } = event
    console.log(event)
    const getDatePart = (date, options, locales = 'en-US') => {
        return date.toLocaleDateString(locales, options)
    }
    const getTime = (date, locales = 'en-US') => {
        return date.toLocaleTimeString(locales, { hour: 'numeric', minute: 'numeric' })
    }
    const [showShare, setShowShare] = useState(false);

    const { dateStr } = useMemo(
        () => {
            const sep = ' \u00B7 '
            const tsep = ' - '
            const dif = localizer.diff(start, localizer.ceil(end, 'day'), 'day')
            let str = [getDatePart(start, { weekday: 'short' }),
            getDatePart(start, { month: 'short', day: 'numeric' })].join(sep)
            if (allDay) {
                if (dif > 1) {
                    str += tsep + [(getDatePart(end, { weekday: 'short' })),
                    getDatePart(end, { month: 'short', day: 'numeric' })].join(sep)
                }
            } else {
                str += sep + getTime(start)
                if (dif > 0) {
                    str += tsep + [getDatePart(end, { weekday: 'short' }),
                    getDatePart(end, { month: 'short', day: 'numeric' })].join(sep)
                }
                str += tsep + getTime(end)
            }
            return { dateStr: str }
        }, [allDay, start, end]
    )

    const { remArr } = useMemo(
        () => ({
            remArr: reminders?.map(rem => {
                const { time, when } = rem
                const { amount, type } = time
                return `${amount} ${type} ${when}`
            })
        }), [reminders]
    )

    const handleCopyEvent = () => {
        let textToCopy = title + '\n' + dateStr
        navigator.clipboard.writeText(textToCopy)
        alert({ content: 'Event copied!', variant: 'success' })
    }

    return (
        <Fragment>
            <div className="se-toolbar">
                <Icon icon="ic:round-content-copy" className='se-icon se-icon-btn se-icon-large' onClick={handleCopyEvent} />
                {editable ?
                    <Fragment>
                        <Link to={`/event/edit/${calendar_type}/${encodeURIComponent(calendar_id)}/${id}`}><Icon icon="ic:round-mode-edit-outline" className='se-icon se-icon-btn se-icon-large' /></Link>
                        <Icon icon="ic:round-delete-outline" className='se-icon se-icon-btn se-icon-large' />
                    </Fragment>
                    :
                    <Fragment>
                        <Link to={encodeURI(`/event/view/${calendar_type}/${encodeURIComponent(calendar_id)}/${id}`)}><Icon icon="ic:round-remove-red-eye" className='se-icon se-icon-btn se-icon-large' /></Link>
                    </Fragment>
                }
                {/* <Link to="/event/edit" state={{'event': event}}><Icon icon="ic:round-mode-edit-outline" className='se-icon se-icon-btn' /></Link> */}
                {/* <Icon icon="ic:round-share" className='se-icon' /> */}
                <Icon icon="ic:round-send" onClick={() => setShowShare(!showShare)} className='se-icon se-icon-btn se-icon-large' />
                {editable &&
                    <DropdownComp
                        btn={<Link to={`/invite/${encodeURIComponent(calendar_id)}/${id}`}><Icon icon="ic:round-more-vert" className='se-icon se-icon-btn se-icon-large' /></Link>}
                        items={[<SeBlock icon='ic:round-people-alt' text={['Invite friend']} />]}>
                    </DropdownComp>
                }

                <Icon icon="ic:round-close" onClick={onClose} className='se-icon se-close se-icon-btn se-icon-large' />
            </div>
            <div className='se-content'>
                <SeBlock title={title} color={color} text={[dateStr, repeat && repeat]} />
                {reminders &&
                    <SeBlock icon='ic:round-notifications-none' text={remArr} />
                }
                <SeBlock icon='ic:round-calendar-month' text={[calendar]} />
                {showShare &&
                    <Telegram solid small message="Share on Telegram"
                        // link={[title, dateStr, calendar].join('\n')}
                        link={window.location.replace('/event/edit/' + calendar_id + '/' + id)}
                    />
                }
            </div>
        </Fragment>)
}

export default SelectedEvent