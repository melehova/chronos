import React, { Component } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import './login.new.component.scss'

import AuthService from "../services/auth.service";

import { withRouter } from '../common/with-router';

const required = value => {
    if (!value) {
        return (
            <div className="alert alert-danger" role="alert">
                This field is required!
            </div>
        );
    }
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
        this.onChangeLogin = this.onChangeLogin.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.changeForm = this.changeForm.bind(this)
        this.state = {
            login: "",
            password: "",
            loading: false,
            message: '',
            class0: true,
            class05: false
        };
    }


    onChangeLogin(e) {
        this.setState({
            login: e.target.value
        });
    }

    onChangePassword(e) {
        this.setState({
            password: e.target.value
        });
    }

    handleLogin(e) {
        e.preventDefault();

        this.setState({
            message: "",
            loading: true
        });

        this.form.validateAll();

        if (this.checkBtn.context._errors.length === 0) {
            AuthService.login(this.state.login, this.state.password).then(
                () => {
                    this.props.router.navigate("/profile");
                    window.location.reload();
                },
                error => {
                    this.setState({
                        loading: false,
                        message: error.response.data
                    });
                }
            );
        } else {
            this.setState({
                loading: false
            });
        }
    }

    changeForm(e) {
        e.preventDefault()
        this.setState(prevState => ({
            class0: !prevState.class0,
            class05: !prevState.class05
        }), () => {
            setTimeout(() => {
                this.setState(prevState => ({
                    class05: !prevState.class05
                }));
            }, 1500);
        });
    }

    render() {
        const { class0, class05 } = this.state
        const class1 = !class0
        return (
            <div className="body">
                <div className="main">
                    <div className={`lcontainer a-container ${class1 && 'is-txl'}`} id="a-container">
                        <form className="form" id="a-form" method="" action="">
                            <h2 className="form_title title">Create Account</h2>
                            <span className="form__span">or use email for registration</span>
                            <input className="form__input" type="text" placeholder="Name"></input>
                            <input className="form__input" type="text" placeholder="Email"></input>
                            <input className="form__input" type="password" placeholder="Password"></input>
                            <button className="form__button button submit">SIGN UP</button>
                        </form>
                    </div>
                    <div className={`lcontainer b-container ${class1 && 'is-txl is-z200'}`} id="b-container">
                        <form className="form" id="b-form" method="" action="">
                            <h2 className="form_title title">Sign in to Website</h2>
                            <span className="form__span">or use your email account</span>
                            <input className="form__input" type="text" placeholder="Email"></input>
                            <input className="form__input" type="password" placeholder="Password"></input>
                            <a className="form__link" href='/'>Forgot your
                                password?</a>
                            <button className="form__button button submit">SIGN IN</button>
                        </form>
                    </div>
                    <div className={`switch ${class05 && 'is-gx'} ${class1 && 'is-txr'}`} id="switch-cnt">
                        <div className={`switch__circle ${class1 && 'is-txr'}`}></div>
                        <div className={`switch__circle ${class1 && 'is-txr'} switch__circle--t`}></div>
                        <div className={`switch__container ${class1 && 'is-hidden'}`} id="switch-c1">
                            <h2 className="switch__title title">Welcome Back !</h2>
                            <p className="switch__description description">To keep connected with us please login with your personal info</p>
                            <button className="switch__button button switch-btn" onClick={this.changeForm}>SIGN IN</button>
                        </div>
                        <div className={`switch__container ${class0 && 'is-hidden'}`} id="switch-c2">
                            <h2 className="switch__title title">Hello Friend !</h2>
                            <p className="switch__description description">Enter your personal details and start journey with us</p>
                            <button className="switch__button button switch-btn" onClick={this.changeForm}>SIGN UP</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);