/* eslint-disable no-unused-vars */
import { Calendar as BigCalendar, momentLocalizer, Views } from 'react-big-calendar'
import React, { useCallback, useState, useMemo, useRef, useEffect } from 'react'
import CustomToolbar from './Toolbar.js';
import PropTypes from 'prop-types'
import Event from './Event.js';
// import ColorPoint from './ColorPoint.js';
import SelectedEvent from './SelectedEvent.js';
import NewEvent from './newEvent.component.js';
import moment from 'moment'
import eventsjson from '../resources/events.json'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css';
import './Calendar.css'
import 'moment-timezone' // or 'moment-timezone/builds/moment-timezone-with-data[-datarange].js'. See their docs
import Menu from './menu.component.js';
import useWindowSize from "../hooks/useWindowSize";
import CalendarService from '../services/calendar-service.js';

// Set the IANA time zone you want to use
// moment.tz.setDefault('Europe/Kyiv')
// moment.tz.setDefault('America/Cancun')

const events = JSON.parse(JSON.stringify(eventsjson))
events.forEach(ev => {
    ev.start = new Date(ev.start)
    ev.end = new Date(ev.end)
});
const localizer = momentLocalizer(moment)
const DnDBigCalendar = withDragAndDrop(BigCalendar)

// to hide selected event 'modal' if click outside of it 
function useOnClickOutside(ref, handler) {
    useEffect(
        () => {
            const listener = (event) => {
                // Do nothing if clicking ref's element or descendent elements
                if (!ref.current || ref.current.contains(event.target)) {
                    return;
                }
                handler(event);
            };
            document.addEventListener("mousedown", listener);
            document.addEventListener("touchstart", listener);
            return () => {
                document.removeEventListener("mousedown", listener);
                document.removeEventListener("touchstart", listener);
            };
        },
        [ref, handler]
    );
}

const Calendar = ({ dayLayoutAlgorithm = 'no-overlap'
    , alert, showMenu,
    // events
}) => {
    const [myEvents, setMyEvents] = useState(events)
    let [view, setView] = useState(Views.MONTH)
    const [selectedEvent, setSelectedEvent] = useState(null)
    const [selectedPosition, setSelectedPosition] = useState({ left: 0, right: 0, top: 0, bottom: 0 })
    const [newEvent, setNewEvent] = useState(null)
    const [newPosition, setNewPosition] = useState({ left: 0, right: 0, top: 0, bottom: 0 })
    const [date, setDate] = useState(new Date())
    const windowSize = useWindowSize();
    const [nationalHolidays, setNationalHolidays] = useState([])
    const [language, setLanguage] = useState(undefined)
    const [region, setRegion] = useState(undefined)
    const [hiddenCalendars, setHiddenCalendars] = useState(localStorage.getItem('hiddenCalendars') || [])
    const [myCalendars, setMyCalendars] = useState([])

    const onNavigate = useCallback((newDate) => setDate(newDate), [setDate])

    const ref = useRef();
    useOnClickOutside(ref, () => {
        setSelectedEvent(null)
    });

    useEffect(() => {
        CalendarService.getCalendarRegion().then(res => {
            setRegion(res.region)
            setLanguage(res.language)
            // console.log(region, language)
        })
        CalendarService.getMyCalendars().then(res => {
            setMyCalendars(res.data.list)
        })
    }, [])

    useEffect(() => {
        CalendarService.getNationalHolidays(region, language /*'en'*/).then(res => {
            setNationalHolidays(res.map(ev => ({
                id: ev.id,
                // start: (new Date(ev.start.date)),
                start: new Date((new Date(ev.start.date)).toISOString().slice(0, -1)),
                end: new Date((new Date(ev.end.date)).toISOString().slice(0, -1)),
                color: '#F66082',
                // end: new Date(ev.end.date),
                allDay: true,
                title: ev.summary,
                description: ev.description.split('\n')[0],
                visibility: ev.visibility,
                calendar: ev.organizer.displayName,
                editable: false,
                calendar_id: ev.creator.email,
                calendar_type: 'national'
            })))
            setMyCalendars(prev => [...prev, {
                _id: res[0].creator.email,
                general: true,
                title: res[0].creator.displayName
            }])
        })
    }, [region, language])

    const onView = useCallback((newView) => setView(newView), [setView])

    const moveEvent = useCallback(
        ({ event, start, end, isAllDay: droppedOnAllDaySlot = false }) => {
            const { allDay } = event
            if (!allDay && droppedOnAllDaySlot) {
                event.allDay = true
            }
            setMyEvents((prev) => {
                const existing = prev.find((ev) => ev.id === event.id) ?? {}
                const filtered = prev.filter((ev) => ev.id !== event.id)
                return [...filtered, { ...existing, start, end, allDay }]
            })
        },
        [setMyEvents]
    )

    const resizeEvent = useCallback(
        ({ event, start, end }) => {
            setMyEvents((prev) => {
                const existing = prev.find((ev) => ev.id === event.id) ?? {}
                const filtered = prev.filter((ev) => ev.id !== event.id)
                return [...filtered, { ...existing, start, end }]
            })
        },
        [setMyEvents]
    )

    const handleSelectSlot = useCallback(
        ({ start, end }) => {
            const title = window.prompt('New Event name')
            if (title) {
                setMyEvents((prev) => [...prev, { start, end, title }])
            }
        },
        [setMyEvents]
    )

    const handleSelectEvent = useCallback(
        (event, e) => {
            e.preventDefault();
            let { left, width, top, height } = e.currentTarget.getBoundingClientRect()
            let right = windowSize.width - (left + width)
            let sesize = Math.min(windowSize.width, Math.max(0.3 * windowSize.width, 320))
            if (left > sesize) {
                setSelectedPosition({
                    left: left - sesize - 7,
                    top: top,
                })
            } else if (right > sesize) {
                setSelectedPosition({
                    right: right - sesize - 7,
                    top: top,
                })
            } else {
                setSelectedPosition({
                    left: windowSize.width * 0.5 - sesize / 2,
                    top: top + height + 7,
                })
            }
            return setSelectedEvent(event)
        },
        [windowSize.width]
    )

    const handleNewEvent = useCallback(
        (props) => {
            const { bounds, box, start, end } = props
            setMyEvents([...myEvents, { start, end, title: 'Untitled', color: '#748080dd' }])
            console.log(props)
            if (bounds) {
                var { left, bottom, top, right } = bounds;
                var height = windowSize.height - top - bottom
                // e.preventDefault();
                let sesize = Math.min(windowSize.width, Math.max(0.3 * windowSize.width, 320))
                if (left > sesize) {
                    setNewPosition({
                        left: left - sesize - 7,
                        top: top,
                    })
                } else if (right > sesize) {
                    setNewPosition({
                        right: right - sesize - 7,
                        top: top,
                    })
                } else {
                    setNewPosition({
                        left: windowSize.width * 0.5 - sesize / 2,
                        top: top + height + 7,
                    })
                }
            } else if (box) {
                setNewPosition({
                    left: '50%',
                    transform: 'translateX(-50%)',
                    top: '50%',
                })
            }

            return setNewEvent({ title: '', start: new Date(start), end: new Date(end) })
        },
        [windowSize.width, windowSize.height, myEvents]
    )
    const clickRef = useRef(null)

    const handleDoubleClick = useCallback((ev) => {
        /**
         * Notice our use of the same ref as above.
         */
        window.clearTimeout(clickRef?.current)
        clickRef.current = window.setTimeout(() => {
            window.location.replace('/event/edit/' + ev.calendar_id + '/' + ev.id)
        }, 250)
    }, [])

    const eventPropGetter = useCallback(
        (event, start, end, isSelected) => {
            return {
                ...((localizer.diff(start, localizer.ceil(end, 'day'), 'day') < 1) && {
                    style: {
                        background: 'transparent',
                    }
                }),
                ...(((event.end - event.start) / (1000 * 3600 * 24) >= 1 || event.allDay || event.end.getDate() !== event.start.getDate()) && {
                    style: {
                        background: event.color,
                    }
                }),
                ...(view === 'day' && {
                    style: {
                        background: event.color,
                        border: 'none',
                    }
                }),
            }
        },
        [view]
    )

    const { components, defaultDate, scrollToTime } = useMemo(
        () => ({
            components: {
                // event: Event.Week,
                toolbar: CustomToolbar,
                // week: {
                //     event: Event.Week
                // }
                month: {
                    event: Event.Month
                },
                day: {
                    event: Event.Day
                    // event: () => <Event.Day alert={alert} />,
                },
                // eventWrapper: EventWrapper,
                // day: { header: MyCustomHeader },
                // week: { header: MyCustomHeader },
                // month: { header: MyCustomHeader },
            },
            defaultDate: new Date(),
            scrollToTime: new Date(),
        }),
        []
    )

    const [eventsList, setEvents] = useState([])

    useEffect(() => {
        setEvents([...myEvents, ...nationalHolidays].filter(ev => !hiddenCalendars.includes(ev.calendar_id)))
    }, [myEvents, nationalHolidays, hiddenCalendars])

    return (
        <div className={`Calendar ${windowSize.width >= 768 ? 'flex-row' : 'flex-column'}`}>
            {selectedEvent &&
                <div
                    id='selected-event'
                    ref={ref}
                    // className='col-4'
                    style={selectedPosition}>
                    <SelectedEvent
                        alert={alert}
                        onClose={(() => setSelectedEvent(null))}
                        event={selectedEvent} />
                </div>
            }
            {newEvent &&
                <div
                    id='new-event'
                    ref={ref}
                    // className='col-4'
                    style={newPosition}>
                    <NewEvent
                        alert={alert}
                        onClose={(() => setNewEvent(null))}
                        event={newEvent} />
                </div>
            }
            {showMenu &&
                <Menu setHiddenCalendars={setHiddenCalendars} hiddenCalendars={hiddenCalendars} myCalendars={myCalendars} />
            }
            <DnDBigCalendar
                className='container'
                components={components}
                dayLayoutAlgorithm={view === 'week' ? 'overlap' : dayLayoutAlgorithm}
                date={date}
                defaultDate={defaultDate}
                // draggableAccessor="isDraggable"
                draggableAccessor={(event) => true}
                // resizebleAccessor={(event) => true}
                endAccessor="end"
                eventPropGetter={eventPropGetter}
                events={eventsList}
                localizer={localizer}
                longPressThreshold={500}
                onDoubleClickEvent={handleDoubleClick}
                onEventDrop={moveEvent}
                onEventResize={resizeEvent}
                onNavigate={onNavigate}
                onSelectEvent={handleSelectEvent}
                onSelectSlot={handleNewEvent}
                onView={onView}
                popup
                resizable
                scrollToTime={scrollToTime}
                selectable
                showAllEvents
                showMultiDayTimes
                startAccessor="start"
                // toolbar={false}
                view={view}
                views={Object.values(Views)}
            />
        </div>
    )
}

Calendar.propTypes = {
    dayLayoutAlgorithm: PropTypes.string,
    alert: PropTypes.func,
}

export default Calendar