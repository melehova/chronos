import React, { Component } from "react";
import { Icon } from '@iconify/react';

export default class IconWithCounter extends Component {
    render() {
        const { counter, icon, size, hFlip, rotate, className, onClick } = this.props
        return (
            <span className={`post_action ${className ? className : ''} ${counter > 0 ? 'with-counter' : ''}`} data-count={counter} onClick={onClick}>
                <Icon icon={icon} width={size} height={size} hFlip={hFlip} rotate={rotate} />
            </span>
        );
    }
}
