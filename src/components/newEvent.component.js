// import PropTypes from 'prop-types'
import { Icon } from '@iconify/react';
import { Fragment, useMemo, useState } from 'react';
import './SelectedEvent.css'
import moment from 'moment'
import { momentLocalizer } from 'react-big-calendar'
import { Link } from "react-router-dom";
import { Telegram } from 'react-social-sharing'
import DropdownComp from './dropdown.component';
import ColorPoint from './ColorPoint';
import './newEvent.component.css'

const localizer = momentLocalizer(moment)

const NeBlock = ({ title, icon, text, color, children }) => {
    return (
        <div className='se-block'>
            <div className='se-marker'>
                {color &&
                    <div className='se-color-marker' style={{ borderColor: color }}></div>
                }
                {icon &&
                    <Icon icon={icon} className='se-icon' />
                }
            </div>
            <div className='se-block-content'>
                {title &&
                    <div className='se-title'>{title}</div>
                }
                <div className='se-text-list'>
                    {text && text.map((item, idx) => (
                        <div className='se-text' key={idx}>{item}</div>
                    ))}
                </div>
            </div>
        </div>
    )
}

const NewEvent = ({ event, onClose, alert }) => {
    const { id, title, start, end, allDay, repeat, reminders, calendar_id, calendar_type = 'u', calendar = 'Personal calendar', editable = true } = event
    console.log(event)
    const colors = [
        '#849DF6',
        '#96D3F4',
        '#A5CC93',
        '#D9BACC',
        '#EF9AAD',
        '#FFC085',
        '#FFF58B'
    ]
    const [color, setColor] = useState(colors[0])
    // const getDatePart = (date, options, locales = 'en-US') => {
    //     return date.toLocaleDateString(locales, options)
    // }
    // const getTime = (date, locales = 'en-US') => {
    //     return date.toLocaleTimeString(locales, { hour: 'numeric', minute: 'numeric' })
    // }
    /*
        const { dateStr } = useMemo(
            () => {
                const sep = ' \u00B7 '
                const tsep = ' - '
                const dif = localizer.diff(start, localizer.ceil(end, 'day'), 'day')
                let str = [getDatePart(start, { weekday: 'short' }),
                getDatePart(start, { month: 'short', day: 'numeric' })].join(sep)
                if (allDay) {
                    if (dif > 1) {
                        str += tsep + [(getDatePart(end, { weekday: 'short' })),
                        getDatePart(end, { month: 'short', day: 'numeric' })].join(sep)
                    }
                } else {
                    str += sep + getTime(start)
                    if (dif > 0) {
                        str += tsep + [getDatePart(end, { weekday: 'short' }),
                        getDatePart(end, { month: 'short', day: 'numeric' })].join(sep)
                    }
                    str += tsep + getTime(end)
                }
                return { dateStr: str }
            }, [allDay, start, end]
        )
        */


    return (
        <Fragment>
            <div className='ne-content'>
                <div className='ne-color-bar'>
                    {colors.map(c => {
                        return <ColorPoint color={c} />
                    })}
                </div>
                {/* <NeBlock title={title} color={color} text={[dateStr, repeat && repeat]} /> */}
                {/* <NeBlock icon='ic:round-calendar-month' text={[calendar]} /> */}
            </div>
        </Fragment>)
}

export default NewEvent