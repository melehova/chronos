import React, { Component } from 'react';
import axios from 'axios';
import authHeader from '../services/auth-header';
import eventBus from '../common/EventBus';

class Image extends Component {
    constructor({ src, alt, className }) {
        super()
        this.state = {
            src: src,
            source: null,
            alt: alt,
            className: className
        }
    }
    state = { source: null };

    componentDidMount() {
        axios(this.state.src, {
            method: 'GET',
            mode: 'no-cors',
            responseType: "arraybuffer",
            headers: {
                // 'Access-Control-Allow-Origin': 'http://localhost:8080',
                'Content-Type': 'application/json',
                'x-access-token': authHeader()['x-access-token']
            },
            withCredentials: true,
            credentials: 'same-origin',
        }).then(response => {
            const base64 = window.btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    '',
                ),
            );
            this.setState({ source: "data:;base64," + base64 });
        })
            .catch(error => {
                if (error.response.status === 401) {
                    eventBus.dispatch('logout')
                }
            })
    }

    render() {
        const { alt, source, className } = this.state

        return <img src={source} alt={alt} className={className} />;
    }
}

export default Image