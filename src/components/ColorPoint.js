import PropTypes from 'prop-types'

const ColorPoint = ({color}) => {
    return (<div className="color-point" style={{background: color}}></div>)
}

ColorPoint.propTypes = {
    color: PropTypes.string,
}

export default ColorPoint
