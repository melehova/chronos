import './calendar.logo.component.css'
import { Icon } from '@iconify/react'
import { Link } from 'react-router-dom'

const CalendarLogo = () => {
    const day = (new Date()).getDate()
    return (<Link to={'/calendar'} className='c-logo'>
        <Icon icon='ion:calendar-clear-outline' width={40} height={40} color="#C93D48" className='c-logo-icon'/>
        <div className='c-logo-day'>{day}</div>
    </Link>)
}

export default CalendarLogo