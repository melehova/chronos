import Dropdown from 'react-bootstrap/Dropdown';
import { Icon } from '@iconify/react';
import DropdownButton from 'react-bootstrap/DropdownButton';
import './dropdown.component.css'

const DropdownComp = ({ btn, items }) => {
    return (
        <div className='dropdown-wrapper'>
            {btn}
            <DropdownButton id="dropdown-item-button" title="">
                {items && items.map((i, idx) => {
                    return (<Dropdown.ItemText as='button' key={idx}>{i}</Dropdown.ItemText>)
                })}
            </DropdownButton>
        </div>
    )
}

export default DropdownComp