import './menu.component.css'
import { motion, AnimatePresence } from "framer-motion"
import useWindowSize from "../hooks/useWindowSize";
import { useState } from 'react';
import { useEffect } from 'react';
import calendarService from '../services/calendar-service';
import { Fragment } from 'react';


const sideVariants = {
    closed: {
        transition: {
            staggerChildren: 0.2,
            staggerDirection: -1
        }
    },
    open: {
        transition: {
            staggerChildren: 0.2,
            staggerDirection: 1
        }
    }
};

const itemVariants = {
    closed: {
        opacity: 0
    },
    open: { opacity: 1 }
};

const Menu = ({ setHiddenCalendars, hiddenCalendars, myCalendars }) => {
    const windowSize = useWindowSize();
    const initial = windowSize.width >= 768 ? { width: 0 } : { height: 0 }
    const animate = windowSize.width >= 768 ? { width: '25%' } : { height: 'max-content' }
    const exit = windowSize.width >= 768 ? {
        width: 0,
        transition: { delay: 0.7, duration: 0.3 }
    } : {
        height: 0,
        transition: { delay: 0.7, duration: 0.3 }
    }

    const [showCalendarList, setShowCalendarList] = useState(false)

    const links = [
        {
            name: "My calendars", id: 1, onClick: () => {
                setShowCalendarList(!showCalendarList)
            }, to: 'showCalendarList'
        },
        { name: "About", to: "#", id: 2 },
        { name: "Blog", to: "#", id: 3 },
        { name: "Contact", to: "#", id: 4 }
    ];

    const handleHideShow = (e) => {
        const id = e.target.id
        if (hiddenCalendars.includes(id)) {
            console.log(1)
            setHiddenCalendars(prev => prev.filter(el => el !== id))
        } else {
            console.log(2)
            setHiddenCalendars(prev => [...prev, id])
        }
        console.log(id, hiddenCalendars)
    }

    return (
        <AnimatePresence>
            <motion.aside
                initial={initial}
                animate={animate}
                exit={exit}>
                <motion.ul
                    className="container menu"
                    initial="closed"
                    animate="open"
                    exit="closed"
                    variants={sideVariants}
                >
                    {links.map(({ name, to, id, onClick }) => (
                        <Fragment>
                            <motion.li
                                whileHover={{
                                    scale: 1.005,
                                    transition: { duration: 0.1 },
                                }}
                                key={id}
                                onClick={onClick}
                                variants={itemVariants}>
                                {name}
                            </motion.li>
                            {showCalendarList && to === 'showCalendarList' &&
                                <ul>
                                    {myCalendars.map(cal => {
                                        return (<li
                                            key={cal._id || cal.calendar_id}
                                            id={cal._id || cal.calendar_id}
                                            onClick={handleHideShow}>
                                            {cal.title}
                                        </li>)
                                    })}
                                </ul>
                            }
                        </Fragment>
                    ))}
                </motion.ul>
            </motion.aside>
        </AnimatePresence>
    )
}

export default Menu