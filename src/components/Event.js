import PropTypes from 'prop-types'
import { Fragment, useEffect, useState } from 'react'
import moment from 'moment'
import { momentLocalizer } from 'react-big-calendar'

const localizer = momentLocalizer(moment)

const Event = {
    Day: ({ event }) => {
        const { start, title, end } = event
        const startStr = start.toLocaleTimeString(undefined, {
            hour: 'numeric',
            minute: 'numeric',
        }) || null
        const endStr = end.toLocaleTimeString(undefined, {
            hour: 'numeric',
            minute: 'numeric',
        }) || null
        return (
            <Fragment>
                <div title={`${startStr} - ${endStr}: ${title}`}>
                    <span>
                        <div className="d-inline">{title}</div>
                    </span>
                </div>
            </Fragment >
        )
    },
    Month: ({ event }) => {
        const { start, allDay, title, end, color, isAllDay } = event
        const duration = localizer.diff(start, localizer.ceil(end, 'day'), 'day')
        const startStr = start.toLocaleTimeString(undefined, {
            hour: 'numeric',
            minute: 'numeric',
        }) || null
        const endStr = end.toLocaleTimeString(undefined, {
            hour: 'numeric',
            minute: 'numeric',
        }) || null

        const [screenSize, setScreenSize] = useState(window.innerWidth)

        const updatePredicate = () => {
            setScreenSize(window.innerWidth)
        }

        useEffect(() => {
            updatePredicate()
            window.addEventListener("resize", updatePredicate);
            return () => {
                window.removeEventListener("resize", updatePredicate);
            }
        })

        return (
            <Fragment>
                {(duration < 1)
                    ? <span>
                        {screenSize < 576
                            ? <div className='color-point full' style={{ backgroundColor: color }}></div>
                            : <div className='color-point d-inline-block me-2' style={{ backgroundColor: color }}></div>
                        }
                        {startStr && screenSize > 992 &&
                            <div className="d-inline me-2">{startStr}</div>
                        }
                        {screenSize >= 576 &&
                            <span>{title}</span>
                        }
                    </span>
                    : <Fragment>
                        {(isAllDay || allDay)
                            ? <Fragment>
                                <div title={`${startStr} - ${endStr}: ${title}`}>
                                    <div className="d-inline">{title}</div>
                                </div>
                            </Fragment>
                            : <Fragment>
                                <div title={`${startStr} - ${endStr}: ${title}`}>
                                    <span>
                                        {startStr && screenSize > 992 &&
                                            <div className="d-inline me-2">{startStr}</div>
                                        }
                                        <div className="d-inline">{title}</div>
                                    </span>
                                </div>
                            </Fragment>}
                    </Fragment>
                }
            </Fragment>
        )
    },
    Week: ({ event }) => {
        const { start, allDay, title, end, isAllDay } = event

        /*<span>
                            <div title={`${startStr} - ${endStr}: ${title}`} class="rbc-event">
                                <div class="rbc-addons-dnd-resizable">
                                    <div class="rbc-addons-dnd-resize-ns-anchor">
                                        <div class="rbc-addons-dnd-resize-ns-icon">
                                        </div>
                                    </div>
                                    <div class="rbc-event-label">{startStr} – {endStr}</div>
                                    <div class="rbc-event-content">{title}</div>
                                    <div class="rbc-addons-dnd-resize-ns-anchor">
                                        <div class="rbc-addons-dnd-resize-ns-icon">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </span> */
        return (
            <span className='allday'>
                {(allDay || isAllDay || localizer.diff(start, localizer.ceil(end, 'day'), 'day') === 1)
                    ? start.toLocaleTimeString('en-US', {
                        hour: 'numeric',
                        minute: 'numeric',
                        // dayPeriod: 'short'
                    })
                    : 'not all day'
                }
                <strong> {title}</strong>
                {/* {event.desc && ':  ' + event.desc} */}
                {/* {event.allDay || isAllDay ? '' : ''} */}
                {/* {event.allDay && isAllDay && 'allday'} */}
            </span>
        )
    }
}

Event.Week.propTypes = {
    event: PropTypes.object,
}

export default Event