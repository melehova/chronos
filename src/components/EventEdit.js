/* eslint-disable no-unused-vars */
import { useEffect, useReducer, useState } from 'react';
import Button from 'react-bootstrap/Button';
import { Icon } from '@iconify/react';
import { Link, useParams } from 'react-router-dom'
import getEventById from '../services/getEventById';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './EventEdit.css'
import calendarService from '../services/calendar-service';

const CategoryButtons = ({ category, onClick, ...props }) => {
    return (
        <>
            {['arrangment', 'reminder', 'task'].map(cat => {
                return <Button type='button'
                    variant='success'
                    onClick={onClick}
                    {...props}
                    className={category === cat && 'active'}
                    value={cat}
                    key={cat}>{cat}</Button>
            })}
        </>
    )
}

const EventEdit = ({ alert }) => {
    const { id: event_id, calendar_type, action } = useParams()
    const calendar_id = decodeURIComponent(useParams().calendar_id)
    const [event, setEvent] = useState({})
    const [canEdit, setCanEdit] = useState(true)
    // const [event, setEvent] = useState(getEventById(id))
    const [titleWidth, setTitleWidth] = useState(0 + 'ch')

    const DateReducer = (state, action) => {
        const { type, date, n, dateType } = action
        switch (type) {
            case 'initStart':
                return { ...state, start: date }
            case 'initEnd':
                return { ...state, end: date }
            case 'set':
                const [h, m] = n.split(':').map(n => parseInt(n))
                const time = (h * 60 + m) * 60 * 1000
                switch (dateType) {
                    case 'start':
                        let nstart = new Date(state.start)
                        nstart.setHours(h)
                        nstart.setMinutes(m)
                        return {
                            ...state,
                            start: nstart,
                            end: new Date(state.end.getTime() + (nstart - state.start)),
                        }
                    case 'end':
                        let nend = new Date(state.end)
                        nend.setHours(h)
                        nend.setMinutes(m)
                        return {
                            ...state,
                            end: nend
                        }
                    default:
                        break
                }
                break
            case 'setAllDay':
                return {
                    ...state,
                    start: new Date((new Date(state.start)).toISOString().slice(0, -1)),
                    end: new Date((new Date(state.end)).toISOString().slice(0, -1)),
                }
            default:
                break
        }
    }

    const handleCheckAllDay = () => {
        setEvent({
            ...event,
            allDay: !event?.allDay,
        })
        dispatchDate({ type: 'setAllDay' })
    }

    useEffect(() => {
        calendarService.getEvent(calendar_id, event_id, calendar_type)
            .then(res => {
                console.log(res)
                setEvent(res)
            })
    }, [calendar_id, event_id, calendar_type])

    const [date, dispatchDate] = useReducer(DateReducer, { start: new Date(), end: new Date() })
    useEffect(() => {
        setTitleWidth(event?.title?.length || 0 + 'ch')
    }, [event.title])

    useEffect(() => {
        setTitleWidth(event?.title?.length)
        dispatchDate({ type: 'initStart', date: event?.start ? new Date(event?.start) : undefined })
        dispatchDate({ type: 'initEnd', date: event?.end ? new Date(event?.end) : undefined })
    }, [event])

    useEffect(() => {
        setCanEdit(action === 'edit' && event.editable)
    }, [event, action])

    const handleChangeTitle = (e) => {
        if (canEdit)
            setEvent({
                ...event,
                title: e.target.value
            })
    }

    const handleChangeCategory = (e) => {
        if (canEdit)
            setEvent({
                ...event,
                category: e.target.value
            })
    }

    const handleSubmit = (e) => {
        // send put request
        e.preventDefault()
    }

    const handleChangeTime = (e) => {
        if (canEdit)
            dispatchDate({ type: 'set', dateType: e.target.name, n: e.target.value })
    }

    useEffect(() => {
        if (!canEdit && action === 'edit') {
            // window.location.replace('edit', 'view')
            window.location.replace(encodeURI('/event/view/' + calendar_type + '/' + encodeURIComponent(calendar_id) + '/' + event_id))
        }
    })

    const get00x00 = (date = new Date()) => {
        return ('0' + date.getHours()).substr(-2) + ':' + ('0' + date.getMinutes()).substr(-2)
    }
    const { start, end } = date
    const dur = (new Date(end - start)) / 1000 // seconds
    const m = (dur / (60)) % 60
    const d = Math.trunc(dur / (60 * 60 * 24))
    const h = Math.trunc((dur / (60 * 60)) % 24)
    const minutes = m > 0 ? m + 'm' : ''
    const days = d > 0 ? d + 'd' : ''
    const hours = h > 0 ? h + 'h' : ''

    return (
        <div className='container m-1 m-md-2 m-lg-2'>
            <form onSubmit={handleSubmit} className='form'>
                <div className='header'>
                    <Link to={'/calendar'}><Icon icon="ic:round-close" className='se-icon se-close se-icon-btn se-icon-large' /></Link>
                    <input value={event.title} onChange={handleChangeTitle} readOnly={!canEdit} style={{ width: titleWidth }}></input>
                    {action === 'edit' && event.editable &&
                        <Button type='submit' variant="success">Save</Button>
                    }
                </div>
                <div className='category'>
                    <Icon icon="material-symbols:category-outline-rounded" className='se-icon se-icon-large' />
                    <CategoryButtons category={event.category || 'arrangment'} disabled={!canEdit} onClick={handleChangeCategory} />
                </div>
                <div className='time flex-column align-items-start flex-md-row'>
                    <Icon icon="ic:baseline-access-time" className='se-icon se-icon-large' />
                    <div className='d-flex flex-column flex-md-row align-items-center align-self-stretch'>

                        <div className='d-flex flex-lg-row flex-column align-items-md-end align-items-center gap-2'>
                            <DatePicker selected={start} disabled={!canEdit} onChange={(date) => { if (canEdit) dispatchDate({ type: 'initStart', date: date }) }} />
                            {!event.allDay &&
                                <input type='time' name='start' disabled={!canEdit} required='required' value={get00x00(start)} onChange={handleChangeTime} size='5' />
                            }
                        </div>
                        -
                        <div className='d-flex flex-lg-row flex-column gap-2 align-items-md-start align-items-center'>
                            {!event.allDay &&
                                <input type='time' name='end' disabled={!canEdit} required='required' min={get00x00(start)} value={get00x00(end)} onChange={handleChangeTime} />
                            }
                            <DatePicker selected={end} disabled={!canEdit} onChange={(date) => { if (canEdit) dispatchDate({ type: 'initEnd', date: date }) }} />
                        </div>
                    </div>
                    <div>duration {days} {hours} {minutes}</div>
                </div>
                <div className='time-add-ons' >
                    <div className='blank-icon se-icon-large' aria-hidden={true} ></div>
                    <label htmlFor='allDay' className='check-label'>
                        <input type={'checkbox'} name='allDay' id='allDay' disabled={!canEdit} onChange={handleCheckAllDay} checked={event.allDay} />
                        <Icon className='check-icon' icon={`material-symbols:check-box-outline-${event?.allDay ? 'rounded' : 'blank'}`} aria-hidden={true} width='22' height='22' />
                        Whole day</label>
                </div>
            </form>
        </div>
    )
}

export default EventEdit
