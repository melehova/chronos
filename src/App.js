import Alert from 'react-bootstrap/Alert';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useEffect, useReducer, useRef, useState } from 'react';
import { Routes, Route, Link, useLocation } from "react-router-dom";
import { useCycle } from "framer-motion";

import EventEdit from './components/EventEdit';
import Calendar from './components/Calendar';
import IconWithCounter from './components/icon-with-counter-component';
import LogoComponent from './components/logo-component'
import Login from './components/login.component';
import LoginNewComponent from './components/login.new.component';
import Register from './components/register.component';
import Profile from './components/profile.component';
import CalendarLogo from './components/calendar.logo.component';
import EventBus from './common/EventBus';
import AuthService from "./services/auth.service";
import Form from 'react-bootstrap/Form';

function App() {
  const [currentUser, setCurrentUser] = useState(undefined)
  const [showMenu, cycleShowMenu] = useCycle(true, false);
  /// set into session

  function alertReducer(state, action) {
    switch (action.type) {
      case 'close':
        return { show: false, content: '' }
      default:
        return { show: true, content: action.content, variant: action.variant ? action.variant : state.variant }
    }
  }

  const [alert, dispatchAlert] = useReducer(alertReducer, { show: false, content: '', variant: 'primary' });
  const ref = useRef(null);

  const logOut = () => {
    AuthService.logout();
    setCurrentUser(undefined)
  }
  const location = useLocation();

  useEffect(() => {
    setCurrentUser(AuthService.getCurrentUser())

    const timeId = setTimeout(() => {
      // After 3 seconds set the show value to false
      dispatchAlert({ type: 'close' })
    }, 3000)



    EventBus.on("logout", () => {
      logOut();
    });

    return () => {
      clearTimeout(timeId)
      EventBus.remove("logout");
    }
  }, [alert.show])


  return (
    <>
      <nav className="navbar navbar-expand navbar-light-green bg-light">
        <div className="container">
          <div className="navbar-brand">
            {currentUser && location.pathname === '/calendar' &&
              <IconWithCounter icon='ic:round-menu' size='30' className='iconify-circle menu-btn iconify-btn' onClick={cycleShowMenu} />
            }
            <Link to={"/"}>
              <LogoComponent />
            </Link>
            {currentUser &&
              <CalendarLogo />
            }
          </div>
          <div className="navbar-nav d-flex justify-content-between flex-fill">
            {/* {currentUser && (
              <li className="nav-item">
                <NavLink to={"/user"} className="nav-link">
                  <IconWithCounter icon='fa6-solid:user-group' size='28' />
                </NavLink>
              </li>
            )} */}
            <div></div>
            {currentUser ? (
              <div className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to={"/profile"} className="nav-link profile">
                    <div className="profile-info">
                      <span className="profile-login">{currentUser.login}</span>
                    </div>
                    <img src={`https://avatars.dicebear.com/api/pixel-art/${currentUser.login}.svg`} alt={`${currentUser.login} avatar`} className="profile-avatar" />
                  </Link>
                </li>
                <li className="nav-item">
                  <a href="/login" className="nav-link" onClick={logOut}>
                    LogOut
                  </a>
                </li>
              </div>
            ) : (
              <div className="navbar-nav align-items-center ms-auto">
                <li className="nav-item">
                  <Link to={"/login"} className="nav-link dark">
                    Sign In
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/register"} className="nav-link">
                    Sign Up
                  </Link>
                </li>
              </div>
            )}
          </div>
          {currentUser &&
            <div className="nav-item ">
              <Form.Control type="text" placeholder="Search" />
            </div>
          }
        </div>
      </nav>
      <Routes>
        <Route path='/'>
          <Route path='event/:action/:calendar_type/:calendar_id/:id' element={<EventEdit alert={dispatchAlert} />} />
          <Route path='login' element={<Login alert={dispatchAlert} />} />
          <Route path="register" element={<Register alert={dispatchAlert} />} />
          <Route path="profile" element={<Profile alert={dispatchAlert} />} />
        </Route>
        <Route path='calendar'>
          <Route index element={<Calendar alert={dispatchAlert} showMenu={showMenu}/>} />
        </Route>
      </Routes>
      <div className="App">
        <Alert
          key={alert.variant}
          ref={ref}
          show={alert.show}
          variant={alert.variant}
          style={alert.style}
          dismissible
          onClose={() => dispatchAlert({ type: 'close' })}>
          {alert.content}
        </Alert>
      </div>
    </>

  );
}

export default App;
